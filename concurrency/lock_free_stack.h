#pragma once
#include <atomic>
#include <memory>

template<typename T>
struct snode
{
   std::shared_ptr<T> data;
   snode* next;
   snode(T const& _data)
      : data(std::make_shared<T>(_data))
   {}
   snode* get_last()
   {
      snode* last = this;
      while(snode* const next = last->next)
      {
         last = next;
      }
      return last;
   }
};

template<typename T>
class cleaner
{
   using node = snode<T>;
   std::atomic<unsigned> threads_in_pop;
   std::atomic<node*> to_be_deleted;

   static void delete_nodes(node* nodes)
   {
      while(nodes)
      {
         node* next = nodes->next;
         delete nodes;
         nodes = next;
      }
   }

   void chain_pending_nodes(node* first, node* last)
   {
      last->next = to_be_deleted;
      while(!to_be_deleted.compare_exchange_weak(last->next, first));
   }
   
   void chain_pending_node(node* n)
   {
      chain_pending_nodes(n, n);
   }

   void try_recalm(node* one_node)
   {
      if(threads_in_pop == 1)
      {
         node* nodes_to_delete = to_be_deleted.exchange(nullptr);   // забираем на обработку
         const auto th_cnt = --threads_in_pop;   // список ожидания нам уже не нужен
         if(!th_cnt)
         {  // пока получали список для обработки, никто не успел зайти в pop
            delete_nodes(nodes_to_delete);
         }
         else if(nodes_to_delete)
         {  // список не пустой
            chain_pending_nodes(nodes_to_delete, nodes_to_delete->get_last());   // возвращаем, не успели обработать
         }
         // в этом потоке мы получили  old_head
         // и при отдачи результата остались одни
         // можно удалить
         delete one_node;
         return;
      }
      chain_pending_node(one_node);   // добавляем к списку ожидающих "обработаннный элемент"
       --threads_in_pop;   // список ожидания нам уже не нужен
   }

public:
   void go_in()
   {
      ++threads_in_pop;
   }
   void go_out(node* node_to_delete)
   {
      try_recalm(node_to_delete);
   }
};

template<class T>
class lock_free_stack
{
   using node = snode<T>;
private:
   std::atomic<node*> head;
   cleaner<T> the_cleaner;
public:
   void push(T const& data)
   {
      node* const new_node = new node(data);
      new_node->next = head.load();
      while(!head.compare_exchange_weak(new_node->next, new_node));
   }
   std::shared_ptr<T> pop()
   {
      the_cleaner.go_in();
      node* old_head = head.load();
      while(old_head 
            && !head.compare_exchange_weak(old_head, old_head->next));
      std::shared_ptr<T> res;
      if(old_head)
      {
         res.swap(old_head->data);
      }
      the_cleaner.go_out(old_head);
      return res;
   }
};

