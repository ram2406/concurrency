#include <iostream>
#include "lock_free_stack.h"

using namespace std;

int main()
{
   lock_free_stack<int> st;
   st.push(1);
   st.push(2);
   st.push(3);
   int check = *st.pop() ;
   cout << "Hello World! " <<  check << endl;
   return 0;
}
